#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <strings.h>
#define BUFLEN 3

int main() {
    int descriptor;
    if ((descriptor = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("Error create socket");
        exit(1);
    }
    printf("Descriptor: %d\n", descriptor);

    struct sockaddr_in servInfo;
    //ZeroMemory(&servInfo, sizeof(servInfo));
    servInfo.sin_family = AF_INET;
    servInfo.sin_port = 0;
    servInfo.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(descriptor, (struct sockaddr*)&servInfo, sizeof(servInfo))) {
        perror("Error bind socket");
        exit(1);
    }


    socklen_t socklen = sizeof(servInfo);
    if (getsockname(descriptor, (struct sockaddr *)&servInfo, &socklen) == -1) {
        perror("Error getsockname");
        exit(1);
    }

    printf("IP: %s\n", inet_ntoa(servInfo.sin_addr));
    printf("Port: %d\n", ntohs(servInfo.sin_port));

    struct sockaddr_in cliInfo;
    //ZeroMemory(&servInfo, sizeof(servInfo));
    char buf[BUFLEN];
    int packet_size = 0;
    socklen_t socket_length;
    
    while (1) {
        socket_length = sizeof(cliInfo);
        bzero(&buf, BUFLEN);
        packet_size = recvfrom(descriptor, &buf, BUFLEN, 0, (struct sockaddr *)&cliInfo, &socket_length);
        if (packet_size < BUFLEN) {
            perror("Error receive message");
            exit(1);
        }
        printf("Client IP: %s\n", inet_ntoa(cliInfo.sin_addr));
        printf("Client Port: %d\n", ntohs(cliInfo.sin_port));
        printf("Client Message: %s\n", buf);
        printf("Client Packet size: %d\n\n", packet_size);

    }
}