#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#define BUFLEN 3

int main(int argc, char *argv[]) {
    if (argc < 4) {
        printf("Error args:\n");
        printf("./client [IP] [Port] [Count message]\n");
        exit(1);
    }

    int descriptor;
    if ((descriptor = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("Error create socket");
        exit(1);
    }
    printf("Descriptor: %d\n", descriptor);

    struct sockaddr_in cliInfo;
    //ZeroMemory(&cliInfo, sizeof(cliInfo));
    cliInfo.sin_family = AF_INET;
    cliInfo.sin_port = 0;
    cliInfo.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(descriptor, (struct sockaddr*)&cliInfo, sizeof(cliInfo))) {
        perror("Error bind socket");
        exit(1);
    }

    socklen_t socklen = sizeof(cliInfo);
    if (getsockname(descriptor, (struct sockaddr *)&cliInfo, &socklen) == -1) {
        perror("Error getsockname");
        exit(1);
    }

    printf("IP: %s\n", inet_ntoa(cliInfo.sin_addr));
    printf("Port: %d\n", ntohs(cliInfo.sin_port));

    struct sockaddr_in servInfo;

    servInfo.sin_family = AF_INET;
    servInfo.sin_port = htons(atoi(argv[2]));

    inet_pton(AF_INET, argv[1], &servInfo.sin_addr);
    printf("Start send message\n");

    char buf[2];

    for (int i = 0; i < atoi(argv[3]); i++) {
        sprintf(buf, "%d", i);
        if (sendto(descriptor, buf, BUFLEN, 0, (struct sockaddr *)&servInfo, sizeof(servInfo)) < 0) {
            perror("Error send message\n");
            exit(1);
        }
        printf("Success send %d\n", i);
        sleep(i);
    }
    printf("Successfully!\n");
    close(descriptor);

}