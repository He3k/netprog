#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>

#define BUFLEN 81

void reaper(int sig) {
    int status;
    while (wait3(&status, WNOHANG, (struct rusage *)0) >= 0);
}

void buffWork(int socket) {
    char buf[BUFLEN] = {0};
    int msg_l;
    // while(1) {
    //     ssize_t readSize = read(socket, buf, sizeof(buf));
    //     if (readSize < 0) {
    //         perror("Fail read\n");
    //         continue;
    //     }
    //     if (readSize = 0) {
    //         close(socket);
    //         printf("1223");
    //         break;
    //     }
    //     buf[readSize] = '\0';
    //     printf("SERVER: сообщение: %s\n", buf);
    //     printf("SERVER: size buf %ld\n", sizeof(buf));
    //     write(socket, buf, strlen(buf));
    // }
    // return;
    bzero(buf, BUFLEN);
    if ((msg_l = recv(socket, buf, BUFLEN, 0)) < 0) {
        perror("Не получилось прочесть сообщение!\n");
        return;
    }
    printf("SERVER: сообщение: %s\n", buf);
    printf("SERVER: длина сообщения: %d\n", msg_l);
}

int main()
{
    int sid = socket(AF_INET, SOCK_STREAM, 0);

    if (sid < 0) {
        perror("Не удалось создать гнездо!");
        exit(1);
    }

    struct sockaddr_in addr_p;

    addr_p.sin_family = AF_INET;
    addr_p.sin_port = 0;

    addr_p.sin_addr.s_addr = htonl(INADDR_ANY);

    //inet_aton("127.0.0.1", &addr_p.sin_addr);

    if (bind(sid, (struct sockaddr *)&addr_p, sizeof(addr_p))) {
        perror("Не получилось присвоить гнезду имя!\n");
        exit(1);
    }

    socklen_t len = sizeof(addr_p);

    if (getsockname(sid, (struct sockaddr *)&addr_p, &len) < 0) {
        perror("Fail getsockname\n");
        exit(1);
    }


    printf("СЕРВЕР: номер порта - %d\n", ntohs(addr_p.sin_port));
    printf("СЕРВЕР: IP - %s\n", inet_ntoa(addr_p.sin_addr));

    if (listen(sid, 3)) {
        perror("Не получилось создать гнездо!\n");
        exit(1);
    }

    // struct sockaddr_in client_addr_p;

    // socklen_t lenC;

    (void) signal(SIGCHLD, reaper);

    while(1) {
        // lenC = sizeof(client_addr_p);
        char buf[BUFLEN];
        int msg_l;


        int sidC = accept(sid, 0,  0);

        if (sidC < 0) {
            perror("Fail accept\n");
            exit(1);
        }

        // f=fork();
        // if (f = 0) { 

        // }

        // buffWork(sidC);
        // close(sidC);

        switch(fork()) {
            case -1:
                perror("fork");
                close(sidC);
                break;
            case 0:
                close(sid);
                while(1) {
                    buf[0] = '\0';
                    if ((msg_l = recv(sidC, buf, BUFLEN, 0)) < 0) {
                        perror("Не получилось прочесть сообщение!\n");
                        exit(1);
                    }
                    if (buf[0] == '\0') {
                        close(sidC);
                        system("ps -aux | grep 'Z'");
                        exit(0);
                    }
                    buf[msg_l] = '\0';
                    printf("SERVER: сообщение: %s\n", buf);
                    printf("SERVER: длина сообщения: %d\n", msg_l);
                    printf("SERVER: идентификатор процесса: %d\n", getpid());
                }
            default:
                close(sidC);
                while(waitpid(-1, NULL, WNOHANG) > 0);



        }
    }
}
