#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <strings.h>

#define BUFLEN 4

int main() {
    int sid = socket(AF_INET, SOCK_DGRAM, 0);

    if (sid < 0) {
        perror("Не удалось создать гнездо!");
        exit(1);
    }

    printf("Кол-во доступных портов: %d\n", sid);

    struct sockaddr_in serv_addr;

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = 0;

    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sid, (struct sockaddr *)&serv_addr, sizeof(serv_addr))) {
        perror("Не получилось присвоить гнезду имя!\n");
        exit(1);
    }

    socklen_t socklen = sizeof(serv_addr);
    if (getsockname(sid, (struct sockaddr *)&serv_addr, &socklen) == -1) {
        perror("Ошибка getsockname");
        exit(1);
    }

    printf("СЕРВЕР: номер порта - %d\n", ntohs(serv_addr.sin_port));
    printf("СЕРВЕР: IP - %s\n", inet_ntoa(serv_addr.sin_addr));

    struct sockaddr_in client_addr_p;

    char buf[BUFLEN];
    int message_length;
    socklen_t len;

    while (1) {
        len = sizeof(client_addr_p);
        bzero(&buf, BUFLEN);

        message_length = recvfrom(sid, &buf, BUFLEN, 0, (struct sockaddr *)&client_addr_p, &len);
        if (message_length < 0) {
            perror("Не получилось принять сообщение!\n");
            exit(1);
        }

        printf("SERVER: IP client - : %s\n", inet_ntoa(client_addr_p.sin_addr));
        printf("SERVER: port client - %d\n", ntohs(client_addr_p.sin_port));
        printf("SERVER: сообщение: %s\n", buf);
        printf("SERVER: длина сообщения: %d\n\n", message_length);
    }
}