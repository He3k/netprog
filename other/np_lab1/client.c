#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (argc < 4) {
        printf("Правильный вызов программы:\n");
        printf("./client [IP-адрес сервера] [порт сервера] [сообщение]\n");
        exit(1);
    }

    int sid = socket(AF_INET, SOCK_DGRAM, 0);
    if (sid < 0) {
        perror("Не удалось создать гнездо\n");
        exit(1);
    }

    struct sockaddr_in client_addr;

    client_addr.sin_family = AF_INET;
    client_addr.sin_port = 0;
    client_addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sid, (struct sockaddr *)&client_addr, sizeof(client_addr))) {
        perror("Не получилось присвоить гнезду имя\n");
        exit(1);
    }

    socklen_t socklen = sizeof(client_addr);
    if (getsockname(sid, (struct sockaddr *)&client_addr, &socklen) == -1) {
        perror("Ошибка getsockname");
        exit(1);
    }

    printf("CLIENT: номер порта - %d\n", ntohs(client_addr.sin_port));
    printf("СLIENT: IP - %s\n", inet_ntoa(client_addr.sin_addr));

    struct sockaddr_in serv_addr;

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2]));

    inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);

    printf("Пересылка сообщения серверу началась!\n");

    char buffer[2];

    for (int i = 0; i < atoi(argv[3]); i++) {
        sprintf(buffer, "%d", i);
        if (sendto(sid, buffer, strlen(argv[2]), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
            perror("Не получилось передать сообщение!\n");
            exit(1);
        }
        printf("\nПередал значение %d\n", i);
        sleep(i);
    }

    printf("Пересылка сообщения серверу завершена!\n");

    close(sid);
}
