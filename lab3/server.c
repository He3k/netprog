#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>
#include <pthread.h>
#include <sys/syscall.h>

#define BUFLEN 81

pthread_mutex_t st_mutex; 

int handler(void *socket) {
    int sock = *(int*) socket;
    int msg_l;
    char buf[BUFLEN];
    FILE *fp;

    pthread_mutex_lock(&st_mutex);

    if ((fp = fopen("messages.txt", "w")) == NULL) {
        perror("Failed to open file");
        close(sock);
        return 0;
    }
    pid_t tId = syscall(__NR_gettid);

    while (1) {
        buf[0] = '\0';
        if ((msg_l = recv(sock, buf, BUFLEN, 0)) < 0) {
            perror("Не получилось прочесть сообщение!\n");
            exit(1);
        }
        if (buf[0] == '\0') {
            break;
        }
        buf[msg_l] = '\0';
        printf("SERVER: сообщение: %s\n", buf);
        printf("SERVER: длина сообщения: %d\n", msg_l);
        printf("SERVER: текущий поток: %d\n", tId);

        fprintf(fp, "%s\n", buf);
        fprintf(fp, "SERVER: сообщение: %s\n", buf);
        fprintf(fp, "SERVER: длина сообщения: %d\n", msg_l);
        fprintf(fp, "SERVER: текущий поток: %d\n\n", tId);
    }

    pthread_mutex_unlock(&st_mutex);

    close(sock);

    fclose(fp);

    return 0;
}

int main()
{
    pthread_t thread;
    pthread_attr_t at;

    int sid = socket(AF_INET, SOCK_STREAM, 0);

    if (sid < 0) {
        perror("Не удалось создать гнездо!");
        exit(1);
    }

    struct sockaddr_in addr_p;

    addr_p.sin_family = AF_INET;
    addr_p.sin_port = 0;

    addr_p.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(sid, (struct sockaddr *)&addr_p, sizeof(addr_p))) {
        perror("Не получилось присвоить гнезду имя!\n");
        exit(1);
    }

    socklen_t len = sizeof(addr_p);

    if (getsockname(sid, (struct sockaddr *)&addr_p, &len) < 0) {
        perror("Fail getsockname\n");
        exit(1);
    }


    printf("СЕРВЕР: номер порта - %d\n", ntohs(addr_p.sin_port));
    printf("СЕРВЕР: IP - %s\n", inet_ntoa(addr_p.sin_addr));

    if (listen(sid, 5)) {
        perror("Не получилось создать гнездо!\n");
        exit(1);
    }

    pthread_attr_init(&at);
    pthread_attr_setdetachstate(&at, PTHREAD_CREATE_DETACHED);
    pthread_mutex_init(&st_mutex, 0);

    while(1) {

        int sidC = accept(sid, 0,  0);

        if (sidC < 0) {
            perror("Fail accept\n");
            exit(1);
        }

        if (pthread_create(&thread, &at, (void *) handler, (void *) &sidC) < 0) {
            perror("Pthread creating failed");
            exit(1);
        }
    }
}